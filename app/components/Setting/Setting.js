import React, {Component} from 'react';
import {View, FlatList, StyleSheet, Text, Image, Button,Dimensions,TouchableOpacity  } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Video from 'react-native-video';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { SearchBar } from 'react-native-elements';

const DATA = [
    {
      id: '1',
      title: 'Account',
      icon:'account'
    },
    {
      id: '2',
      title: 'Notificaciones',
      icon:'bell'
    },
    {
      id: '3',
      title: 'Appearance',
      icon:'eye'
    },
    {
        id: '4',
        title: 'Privacity & Security',
        icon:'lock'
      },
      {
        id: '5',
        title: 'Help and Support',
        icon:'headphones-settings'
      },
      {
        id: '6',
        title: 'About',
        icon:'help-circle'
      },
  ];
  function Item({ title,icon }) {
    return (
        <View style= {styles.container}>
        <View style={{flex:1, flexDirection: 'row'}}>
        <MaterialCommunityIcons style={styles.imageView} name={icon} color={"#000000"} size={40}/>
            <View style= {styles.textView}>
            <Text style= {styles.title}>{title}</Text>
            </View>
            <View style={styles.textvolver}>
            <TouchableOpacity>
            <Image source={require('./boton1.png') } style={{height: 40, width: 40}}/>
        </TouchableOpacity >
        </View>
        </View>
        </View>
    );
  }
  

var { height } = Dimensions.get('window');

export default class Setting extends Component{
    
    constructor(props){
        super(props); 
        this.state = {
          text: "",
          items: DATA,
          data:DATA,
      };        
    }
    renderHeader = () => {    
        return (      
          <SearchBar        
            placeholder="Search for a settings..."        
            lightTheme        
            round        
            //onChangeText={text => this.searchFilterFunction(text)}
            //value= {this.state.text}
            autoCorrect={false}             
          />    
        );  
      };

      renderSeparatorView = () => {
        return (
          <View style={{
              height: 1, 
              width: "100%",
              backgroundColor: "#a9a9a9",
            }}
          />
        );
      };
      searchFilterFunction = text => {
            
        const newData = this.state.items.filter(item => {      
        const itemData = `${item.title.toUpperCase()}`;
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;    
          });
          
          this.setState({ data: newData });
          this.setState({text: text})  
        };
      renderHeader = () => {    
          return (      
            <SearchBar        
              placeholder="Type Here..."        
              lightTheme        
              round        
              onChangeText={text => this.searchFilterFunction(text)}
              value= {this.state.text}
              autoCorrect={false}             
            />    
          );  
        };

    render(){

        return(
            <View style={styles.container}>
            <FlatList
                data={this.state.data}
                renderItem={({ item }) => <Item title={item.title} icon={item.icon} />}
                keyExtractor={item => item.id}
                ListHeaderComponent={this.renderHeader}
                ItemSeparatorComponent={this.renderSeparatorView}
            />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        padding:50,
        justifyContent: 'center',
        alignContent: 'center',
      },
    container: {
      marginTop: 15,
      margin: 20,
    },
    imageView: {
        width: '20%',
        height: 40 ,
        margin: 20,
        borderRadius : 30, 
    },
    textView: {
        width:'60%', 
        textAlignVertical:'center',
        textAlign:'justify',
        justifyContent: 'center',
        fontFamily: 'Pacifico-Regular',
    },
    textvolver:{
        width: '20%',
        justifyContent: 'center'
        
    },
    item: {
      backgroundColor: 'gray',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 8,
    },
    title: {
      fontSize: 20,
      color: '#000000',
      fontWeight: 'bold',
      fontFamily: 'Bangers',
    },
    titleloading: {
        fontSize: 40,
        color: '#c43c00',
        fontWeight: 'bold',
        fontFamily: 'Bangers',
      },
  });