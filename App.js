/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import * as React from 'react';
import { Text, View, Button,TouchableOpacity, StyleSheet } from 'react-native';
import { NavigationContainer,CommonActions  } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Details from './app/components/Details/Details' 
import ConexionFetch from './app/components/conexionFetch/ConexionFetch'
import TopNavigation from './app/components/menu/TopNavigation'
import Login from './app/components/Login/Login'
import Videos from './app/components/Video/Video'
import Setting from './app/components/Setting/Setting'
import ListaVideos from './app/components/ListaVideos/ListaVideos'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

function LoginScreen({ navigation, CommonActions }) {
  return (
    <View>
      <Login navigation={navigation} CommonActions={CommonActions}></Login>
    </View>
    
  );
}
function HomeScreen({ navigation }) {
  return (
    <ConexionFetch navigation={navigation}></ConexionFetch>
  );
}

function DetailsScreen({ route, navigation }) {
  /* 2. Get the param */
  const { title } = route.params;
  const { image } = route.params;
  const { summary } = route.params;

  const titulo = JSON.stringify(title)
  const imagen = JSON.stringify(image)
  const resumen = JSON.stringify(summary)

  return (
    <View>
      <Details titulo={titulo} navigation={navigation} imagen={imagen} resumen={resumen} ></Details>     
    </View>
  );
}
function SettingScreen({ navigation }) {
  return (
    <View>
      <Setting navigation={navigation}></Setting>
    </View>
    
  );
}
function VideoScreen({ navigation }) {
  return (
    <View>
      <ListaVideos navigation={navigation}></ListaVideos>
    </View>
    
  );
}
function DetallesVideo({ navigation,route }) {
  const { title } = route.params;
  const { id } = route.params;

  return (
    <View>
      <Videos id={id} title={title} navigation={navigation}></Videos>
    </View>
    
  );
}

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createMaterialBottomTabNavigator();
const HomeStack = createStackNavigator();
const SettingStack = createStackNavigator();
const VideoStack = createStackNavigator();

/* STACK HOME */
function HomeStackScreen(){
  return (
  <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={HomeScreen} 
        options={({navigation}) => (
          { 
          headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
          title: 'Lista de peliculas',
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerLeft:null,
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 30,
          },
        }
      )}
        />
      <HomeStack.Screen name="Details" component={DetailsScreen} 
        options={({navigation}) => (
          {
          headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
          title: 'Detalles pelicula',
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 30,
          },
        }
        )}
        />
  </HomeStack.Navigator>
  );
}

/* STACK SETTINGS */
function SettingStackScreen(){
  return(
    <SettingStack.Navigator>
      <SettingStack.Screen name="Setting" component={SettingScreen} 
        options={({navigation}) => (
          { 
          headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
          title: 'Configuraciones',
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 30,
          },
        }
      )}
      />
    </SettingStack.Navigator>
  );
}

/* STACK VIDEO */

function VideoStackScreen(){
  return(
    <VideoStack.Navigator>
    <VideoStack.Screen name="Video" component={VideoScreen} 
      options={({navigation}) => (
        { 
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        title: 'Lista Videos Anime',
        headerStyle: {
          backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <VideoStack.Screen name="DetalleVideo" component={DetallesVideo} 
        options={({navigation}) => (
          {
          headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
          title: 'Detalles video',
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 30,
          },
        }
        )}
        />
  </VideoStack.Navigator>
  );
}

/* SCREM TABS */

function MenuTapScreen (){
  return (
    <Tab.Navigator
    initialRouteName="Home"
    tabBarOptions={{
      activeTintColor: '#e91e63',
    }}
    barStyle={{ backgroundColor: '#f4511e' }}>
    <Tab.Screen 
      name="Home" 
      component={HomeStackScreen}
      options = {{
        tabBarLabel: 'Home',
        tabBarIcon: ({color, size}) => (
          <MaterialCommunityIcons name="home" color={color} size={size}/>
        ),
      }}/>
    <Tab.Screen 
      name="Video" 
      component={VideoStackScreen}
      options = {{
        tabBarLabel: 'Video',
        tabBarIcon: ({color, size}) => (
          <MaterialCommunityIcons name="airplay" color={color} size={size}/>
        ),
      }}/>
      <Tab.Screen 
      name="Setting" 
      component={SettingStackScreen}
      options = {{
        tabBarLabel: 'Settings',
        tabBarIcon: ({color, size}) => (
          <MaterialCommunityIcons name="settings-outline"  color={color} size={size}/>
        ),
      }}/>
  </Tab.Navigator>
  );
}

export default function App() {  
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        tabBarOptions={{
          activeTintColor: '#e91e63',
        }}>
        <Stack.Screen 
          name="Login" component={LoginScreen} 
          options={{
            title: 'Login',
            headerShown: false
          }}
          />
        <Stack.Screen 
          name="BotonMenu" 
          component={MenuTapScreen}
          options={{
            headerShown: false
          }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
  
}
const styles = StyleSheet.create({
  
  textSub: {
      width:'70%', 
      textAlignVertical:'center',
      color: '#f4511e',
      textAlign:'justify',
      fontSize: 20,
   
  },
  textTitle:{
      fontWeight: 'bold',
  },
  textView: {
      textAlignVertical:'center',
      color: '#c43c00',
      textAlign:'justify',
      padding: 10
   
  },
  text: {
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: 30,
    color: '#f4511e',
  },
  image: {
      alignItems: 'center',
      justifyContent: 'center',
      padding: 20,
    },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#f4511e',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
  textButon: {
      alignItems: 'center',
      padding: 5,
      fontWeight: 'bold',
      fontSize: 20,
      color: '#fff',
    },
})
