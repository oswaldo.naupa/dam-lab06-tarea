import React, {Component} from 'react';
import {View, FlatList, StyleSheet, TouchableWithoutFeedback, Text, Image, Button,Dimensions,TouchableOpacity  } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Video from 'react-native-video';
import ProgressBar from "react-native-progress/Bar";
import Icon from "react-native-vector-icons/FontAwesome";

function secondsToTime(time) {
  return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
}

var { height } = Dimensions.get('window');
export default class Videos extends Component{
    
    constructor(props){
        super(props);
        this.state = {
          paused: false,
          progress: 0,
          duration: 0,
      };        
    }

    handleMainButtonTouch = () => {
      if (this.state.progress >= 1) {
        this.player.seek(0);
      }
  
      this.setState(state => {
        return {
          paused: !state.paused,
        };
      });
    };
  
    handleProgressPress = e => {
      const position = e.nativeEvent.locationX;
      const progress = (position / 250) * this.state.duration;
      const isPlaying = !this.state.paused;
      
      this.player.seek(progress);
    };
  
    handleProgress = progress => {
      this.setState({
        progress: progress.currentTime / this.state.duration,
      });
    };
  
    handleEnd = () => {
      this.setState({ paused: true });
    };
  
    handleLoad = meta => {
      this.setState({
        duration: meta.duration,
      });
    };    
  
    render(){
      const { width } = Dimensions.get("window");
      const height = width * 0.5625;
      const id = this.props.id

      return(
        <View style={styles.container}>
            <Text style={styles.text}>{this.props.title}</Text>
            { id == 1 ? 
              <Video
              key={this.props.id}
              source={require('./Videos/video' +1+ '.mp4')} // Can be a URL or a local file.
              ref={(ref) => {
                this.player = ref
              }}
              repeat                                      // Store reference
              onBuffer={this.onBuffer}                // Callback when remote video is buffering
              onError={this.videoError} 
              resizeMode="contain"              // Callback when video cannot be loaded
              style={{ width: "100%", height }}
              paused={this.state.paused}
              onLoad={this.handleLoad}
              onProgress={this.handleProgress}
              onEnd={this.handleEnd} 
              />  
              : id == 2 ? 
              <Video
                  key={this.props.id}
                  source={require('./Videos/video' +2+ '.mp4')} // Can be a URL or a local file.
                  ref={(ref) => {
                    this.player = ref
                  }}
                  repeat                                      // Store reference
                  onBuffer={this.onBuffer}                // Callback when remote video is buffering
                  onError={this.videoError} 
                  resizeMode="contain"              // Callback when video cannot be loaded
                  style={{ width: "100%", height }}
                  paused={this.state.paused}
                  onLoad={this.handleLoad}
                  onProgress={this.handleProgress}
                  onEnd={this.handleEnd} 
                  />
                :id == 3 ? 
                <Video
                  key={this.props.id}
                  source={require('./Videos/video' +3+ '.mp4')} // Can be a URL or a local file.
                  ref={(ref) => {
                    this.player = ref
                  }}
                  repeat                                      // Store reference
                  onBuffer={this.onBuffer}                // Callback when remote video is buffering
                  onError={this.videoError} 
                  resizeMode="contain"              // Callback when video cannot be loaded
                  style={{ width: "100%", height }}
                  paused={this.state.paused}
                  onLoad={this.handleLoad}
                  onProgress={this.handleProgress}
                  onEnd={this.handleEnd} 
                  />
                :id == 4 ?
                <Video
                  key={this.props.id}
                  source={require('./Videos/video' +4+ '.mp4')} // Can be a URL or a local file.
                  ref={(ref) => {
                    this.player = ref
                  }}
                  repeat                                      // Store reference
                  onBuffer={this.onBuffer}                // Callback when remote video is buffering
                  onError={this.videoError} 
                  resizeMode="contain"              // Callback when video cannot be loaded
                  style={{ width: "100%", height }}
                  paused={this.state.paused}
                  onLoad={this.handleLoad}
                  onProgress={this.handleProgress}
                  onEnd={this.handleEnd} 
                  />
                :
                <Video
                  key={this.props.id}
                  source={require('./Videos/video' +5+ '.mp4')} // Can be a URL or a local file.
                  ref={(ref) => {
                    this.player = ref
                  }}
                  repeat                                      // Store reference
                  onBuffer={this.onBuffer}                // Callback when remote video is buffering
                  onError={this.videoError} 
                  resizeMode="contain"              // Callback when video cannot be loaded
                  style={{ width: "100%", height }}
                  paused={this.state.paused}
                  onLoad={this.handleLoad}
                  onProgress={this.handleProgress}
                  onEnd={this.handleEnd} 
                  />
                }             
               
             <View style={styles.controls}>
              <TouchableWithoutFeedback onPress={this.handleMainButtonTouch}>
                <Icon name={!this.state.paused ? "pause" : "play"} size={30} color="#FFF" />
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={this.handleProgressPress}>
                <View>
                  <ProgressBar
                    progress={this.state.progress}
                    color="#FFF"
                    unfilledColor="rgba(255,255,255,.5)"
                    borderColor="#FFF"
                    width={250}
                    height={20}
                  />
                </View>
              </TouchableWithoutFeedback>

              <Text style={styles.duration}>
                {secondsToTime(Math.floor(this.state.progress * this.state.duration))}
              </Text>
            </View>
            <View style={styles.button} >
            <TouchableOpacity     onPress={() => {
          /* 1. Navigate to the Details route with params */
          this.props.navigation.navigate('Video');
        }}>
            <Text style={styles.textButon}>Volver</Text>
            </TouchableOpacity > 
            </View>
        </View>
    )      
    }
}

const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      justifyContent: 'center',
     
    },
    controls: {
      backgroundColor: "rgba(0, 0, 0, 0.5)",
      height: 48,
      left: 0,
      bottom: 0,
      right: 0,
      position: "absolute",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-around",
      paddingHorizontal: 10,
    },
    mainButton: {
      marginRight: 15,
    },
    duration: {
      color: "#FFF",
      marginLeft: 15,
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      },
    textSub: {
        width:'70%', 
        textAlignVertical:'center',
        color: '#f4511e',
        textAlign:'justify',
        fontSize: 40,
     
    },
    textTitle:{
        fontWeight: 'bold',
    },
    textView: {
        textAlignVertical:'center',
        color: '#c43c00',
        textAlign:'justify',
        padding: 10
     
    },
    text: {
      alignItems: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      color: '#f4511e',
      padding:40,
      fontFamily: "Pacifico-Regular",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
      },
    button: {
      top: 420,
      backgroundColor: '#f4511e',
      position: "absolute",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-around",
      padding: 5,
      width:150,
      borderRadius: 10
    },
    countContainer: {
      alignItems: 'center',
      padding: 10,
    },
    countText: {
      color: '#FF00FF',
    },
    textButon: {
        alignItems: 'center',
        padding: 5,
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff',
      },
  })