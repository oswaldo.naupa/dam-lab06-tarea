import React, {Component} from 'react';
import {View, FlatList, StyleSheet, Text, Image, Button,Dimensions,TouchableOpacity  } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Video from 'react-native-video';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { SearchBar } from 'react-native-elements';

const DATA = [
    {
      id: '1',
      Titulo: 'Shingeki no Kyojin',
      ubicacion: "require('./Videos/video.mp4')",
      imagen:'https://areajugones.sport.es/wp-content/uploads/2020/04/ataque-a-los-titanes-murallas-min.jpg'
    },
    {
      id: '2',
      Titulo: 'Naruto Shippuden',
      ubicacion: "require('./Videos/video.mp4')",
      imagen:'https://pm1.narvii.com/6868/12d1c8957b508eca684bfda3b2dbacea14d5a789r1-1024-640v2_uhq.jpg'
    },
    {
      id: '3',
      Titulo: 'Dragon Ball Z',
      ubicacion: "require('./Videos/video.mp4')",
      imagen:'https://i.blogs.es/13b932/1579084668_612285_1579085061_noticia_normal/1366_2000.jpg'
    },
    {
      id: '4',
      Titulo: 'Caballeros del Zodiaco',
      ubicacion: "require('./Videos/video.mp4')",
      imagen:'https://3.bp.blogspot.com/-C9L0qyw8mP8/WGGbAnwpSsI/AAAAAAAAB9A/DZdDBlKn4lQYkWWWwdHA5B59i4v94y5bgCEw/s1600/saint%2Bseiya%2Bsaga%2Bde%2Bhades.jpg'
      },
      {
        id: '5',
        Titulo: 'YU-GI-OH',
        ubicacion: "require('./Videos/video.mp4')",
        imagen:'https://www.tierragamer.com/wp-content/uploads/2019/06/Yu-Gi-Oh-Nueva-Serie-450x300.jpg'
        },
  ];
  function Item({ title,imagen,navigation,ubicacion,id }) {
    return (
        <View style= {styles.container}>
        <View style={{flex:1, flexDirection: 'row'}}>
            <Image style={styles.imageView} source = {{uri: imagen}}/>
            <View style= {styles.textView}>
                <Text style= {styles.title}>{title}</Text>
            </View>
            <View style={styles.textvolver}>
            <TouchableOpacity onPress={() => {
                    /* 1. Navigate to the Details route with params */
                    navigation.navigate('DetalleVideo', {id:id, title:title
                    });
                    }}>
            <Image source={require('./boton1.png') } style={{height: 40, width: 40}}/>
        </TouchableOpacity >
        </View>
        </View>
        </View>
    );
  }
  

var { height } = Dimensions.get('window');

export default class ListaVideos extends Component{
    
    constructor(props){
        super(props);
        this.state = {
          text: "",
          items: DATA,
          data:DATA,
      };        
    }
    renderHeader = () => {    
        return (      
          <SearchBar        
            placeholder="Search for a settings..."        
            lightTheme        
            round        
            //onChangeText={text => this.searchFilterFunction(text)}
            //value= {this.state.text}
            autoCorrect={false}             
          />    
        );  
      };

      renderSeparatorView = () => {
        return (
          <View style={{
              height: 1, 
              width: "100%",
              backgroundColor: "#a9a9a9",
            }}
          />
        );
      };
      searchFilterFunction = text => {
            
        const newData = this.state.items.filter(item => {      
        const itemData = `${item.Titulo.toUpperCase()}`;
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;    
          });
          
          this.setState({ data: newData });
          this.setState({text: text})  
        };
      renderHeader = () => {    
          return (      
            <SearchBar        
              placeholder="Type Here..."        
              lightTheme        
              round        
              onChangeText={text => this.searchFilterFunction(text)}
              value= {this.state.text}
              autoCorrect={false}             
            />    
          );  
        };

    render(){

        return(
            <View style={styles.container}>
            <FlatList
                data={this.state.data}
                renderItem={({ item }) => <Item title={item.Titulo} imagen={item.imagen}  ubicacion={item.ubicacion} id={item.id} navigation={this.props.navigation}/>}
                keyExtractor={item => item.id}
                ListHeaderComponent={this.renderHeader}
                ItemSeparatorComponent={this.renderSeparatorView}
                ListHeaderComponent={this.renderHeader}
            />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        padding:50,
        justifyContent: 'center',
        alignContent: 'center',
      },
    container: {
      marginTop: 15,
      margin: 20
    },
    imageView: {
        width: '40%',
        height: 80 ,
        margin: 10,
        borderRadius : 30, 
    },
    textView: {
        width:'40%', 
        textAlignVertical:'center',
        textAlign:'justify',
        justifyContent: 'center',
        fontFamily: 'Pacifico-Regular',
        margin: 10,
    },
    textvolver:{
        width: '20%',
        justifyContent: 'center'
        
    },
    item: {
      backgroundColor: 'gray',
      padding: 10,
      marginVertical: 8,
      marginHorizontal: 8,
    },
    title: {
      fontSize: 20,
      color: '#000000',
      fontWeight: 'bold',
      fontFamily: 'Bangers',
    },
    titleloading: {
        fontSize: 40,
        color: '#c43c00',
        fontWeight: 'bold',
        fontFamily: 'Bangers',
      },
  });